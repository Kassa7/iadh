<?php

namespace App\Admin;

use App\Entity\Fonction;
use App\Entity\Interest;
use App\Entity\Level;
use App\Entity\Organization;
use App\Repository\FonctionRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\Form\Type\DateTimePickerType;
use Sonata\MediaBundle\Form\Type\MediaType;
use Sonata\UserBundle\Form\Type\SecurityRolesType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormTypeInterface;
\Locale::setDefault( 'en' );

final class ContactAdmin extends AbstractAdmin {

    protected $translationDomain = 'SonataUserBundle';

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $showMapper): void
    {
        $showMapper
            ->with('General')
            ->add('name')
            ->add('email')
            ->add('function')
            ->add('dateOfBirth')
            ->add('gender')
            ->add('phone')
            ->add('fax')
            ->add('cv', MediaType::class, array(
                'template' => 'admin/cv_show.html.twig'
            ))
            ->end()
            ->with('Social')
            ->add('facebookUid')
            ->add('facebookName')
            ->end()
        ;
    }
    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper): void
    {
        // define group zoning
        $formMapper
            ->with('General', ['class' => 'col-md-6'])->end()
            ->with('Social', ['class' => 'col-md-6'])->end()
        ;

        $now = new \DateTime();

        $genderOptions = [
            'choices' => \call_user_func(["App\\Entity\\Contact", 'getGenderList']),
            'required' => true,
            'translation_domain' => $this->getTranslationDomain(),
        ];

        // NEXT_MAJOR: Remove this when dropping support for SF 2.8
        if (method_exists(FormTypeInterface::class, 'setDefaultOptions')) {
            $genderOptions['choices_as_values'] = true;
        }

        $formMapper
            ->with('General')
            ->add('name')
            ->add('email')
            ->add('function', TextType::class, [
                'label' => 'form.label_function_fonction'
            ])
            ->add('dateOfBirth', \Sonata\Form\Type\DatePickerType::class, [
                'dp_language' => 'en',
            ])
            ->add('gender', ChoiceType::class, $genderOptions)
            ->add('phone', null, ['required' => false])
            ->add('mobile', null, ['required' => false])
            ->add('fax', null, ['required' => true]);

        $locale = $this->request->getLocale();
        $formMapper->add('organization', ModelType::class, [
            'required' => true,
            'expanded' => false,
            'multiple' => false,
            'property' => 'name',
        ])
            ->add('function', ModelType::class, [
                'required' => true,
                'expanded' => false,
                'multiple' => false,
                'property' => 'name',
            ])
            ->add('level', ModelType::class, [
                'required' => true,
                'expanded' => false,
                'multiple' => false,
                'property' => 'name',
            ])
            ->add('interest', ModelType::class, [
                'required' => true,
                'expanded' => false,
                'multiple' => false,
                'property' => 'name',
            ]);


        $formMapper
            ->add('station')
            ->add('ville')
            ->add('country', CountryType::class, ['required' => false])
            ->add('cv', MediaType::class, array(
                'provider' => 'sonata.media.provider.file',
                'context'  => 'default'
            ))


            ->end()
            ->with('Social')
            ->add('facebookUid', null, ['required' => false])
            ->add('facebookName', null, ['required' => false])
        ;
    }



  protected function configureListFields(ListMapper $listMapper) {

      $listMapper
          ->addIdentifier('name')
          ->add('email')
          ->add('function')
          ->add('phone')
      ;
      $locale = $this->request->getLocale();
      $listMapper
          ->add('function.name')
      ;
  }

    protected function configureBatchActions($actions)
    {
        if (
            $this->hasRoute('edit') && $this->hasAccess('edit') &&
            $this->hasRoute('delete') && $this->hasAccess('delete')
        ) {
            $actions['send email'] = [
                'label'            => $this->trans('batch.sendMails.action'),
                'ask_confirmation' => true
            ];
        }

        return $actions;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $filterMapper): void
    {
        $filterMapper
            ->add('name')
            ->add('email')
            ->add('phone')
        ;
        $locale = $this->request->getLocale();
        $filterMapper
            ->add('function.name')
            ->add('level.name')
            ->add('organization.name')
            ->add('interest.name')
        ;
    }
}