<?php
namespace App\Controller;

use Sonata\AdminBundle\Controller\CRUDController as BaseController;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ContactAdminController extends BaseController
{
/**
* @param ProxyQueryInterface $selectedModelQuery
* @param Request             $request
*
* @return RedirectResponse
*/
public function batchActionSendEmail(ProxyQueryInterface $selectedModelQuery, Request $request = null)
{
$this->admin->checkAccess('edit');
$this->admin->checkAccess('delete');

$modelManager = $this->admin->getModelManager();

//$target = $modelManager->find($this->admin->getClass(), $request->get('idx'));
//    $target = $target[0];
//if ($target === null){
//$this->addFlash('sonata_flash_info', 'flash_batch_merge_no_target');
//
//return new RedirectResponse(
//$this->admin->generateUrl('list', [
//'filter' => $this->admin->getFilterParameters()
//])
//);
//}

$selectedModels = $selectedModelQuery->execute();
    $request->request->set('_sonata_admin','admin.template');


return $this->redirectToRoute(
    'admin_app_email_create',
    array('targets'  => $selectedModels,
    Response::HTTP_MOVED_PERMANENTLY // = 301
));


// do the merge work here
    $response = $this->forward('App\Controller\EmailAdminController::sendMailsAction', [
        'targets'  => $selectedModels,
        '_sonata_admin' => 'admin.email'
    ]);

    // ... further modify the response or return it directly

    return $response;
try {
foreach ($selectedModels as $selectedModel) {
    $this->addFlash('sonata_flash_success', 'flash_batch_merge_success');
}

$modelManager->update($selectedModel);
} catch (\Exception $e) {
$this->addFlash('sonata_flash_error', 'flash_batch_merge_error');

return new RedirectResponse(
$this->admin->generateUrl('list', [
'filter' => $this->admin->getFilterParameters()
])
);
}

$this->addFlash('sonata_flash_success', 'flash_batch_merge_success');

return new RedirectResponse(
$this->admin->generateUrl('list', [
'filter' => $this->admin->getFilterParameters()
])
);
}

// ...
}