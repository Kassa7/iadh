<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sonata\UserBundle\Model\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;


    /**
     * @var \App\Entity\Organization
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Organization", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $organization;

    /**
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param Organization $organization
     */
    public function setOrganization(Organization $organization): void
    {
        $this->organization = $organization;
    }


    /**
     * @var \App\Application\Sonata\MediaBundle\Entity\Media
     *
     * @ORM\ManyToOne(targetEntity="\App\Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="acv_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $cv;

    /**
     * @return \App\Application\Sonata\MediaBundle\Entity\Media
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * @param \App\Application\Sonata\MediaBundle\Entity\Media $cv
     */
    public function setCv(\App\Application\Sonata\MediaBundle\Entity\Media $cv): void
    {
        $this->cv = $cv;
    }


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mobile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $convention;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $adress;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $ville;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $country;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $station;


    /**
     * @var \App\Entity\Interest
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Interest", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="interest_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $interest;

    /**
     * @return Interest
     */
    public function getInterest()
    {
        return $this->interest;
    }

    /**
     * @param Interest $interest
     */
    public function setInterest(Interest $interest): void
    {
        $this->interest = $interest;
    }

    /**
     * @var \App\Entity\Fonction
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Fonction", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="function_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $function;

    /**
     * @return Fonction
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * @param Fonction $function
     */
    public function setFunction(Fonction $function): void
    {
        $this->function = $function;
    }

    /**
     * @var \App\Entity\Level
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Level", cascade={"persist"}, fetch="LAZY")
     * @ORM\JoinColumn(name="level_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $level;

    /**
     * @return Level
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param Level $level
     */
    public function setLevel(Level $level): void
    {
        $this->level = $level;
    }

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $facebookUid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $facebookName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $gender = UserInterface::GENDER_UNKNOWN; // set the default to unknown

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $dateOfBirth;

    /**
     * @Assert\Email()
     * @Assert\Length(max=255)
     * @Assert\NotNull()
     * @ORM\Column(type="string", length=255)
     */
    protected $email;

    /**
     * @ORM\Column(type="datetime", length=255, nullable=true)
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", length=255, nullable=true)
     */
    protected $updatedAt;


    /**
     * Returns a string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName() ?: '-';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getConvention(): ?string
    {
        return $this->convention;
    }

    public function setConvention(string $convention): self
    {
        $this->convention = $convention;

        return $this;
    }


    public function getAdress(): ?string
    {
        return $this->adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }


    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }


    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getStation(): ?string
    {
        return $this->station;
    }

    public function setStation(string $station): self
    {
        $this->station = $station;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt(\DateTime $createdAt = null)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * {@inheritdoc}
     */
    public function setUpdatedAt(\Datetime $updatedAt = null)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * {@inheritdoc}
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPhone()
    {
        return $this->phone;
    }
    /**
     * {@inheritdoc}
     */
    public function setFacebookName($facebookName)
    {
        $this->facebookName = $facebookName;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getFacebookName()
    {
        return $this->facebookName;
    }

    /**
     * {@inheritdoc}
     */
    public function setFacebookUid($facebookUid)
    {
        $this->facebookUid = $facebookUid;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getFacebookUid()
    {
        return $this->facebookUid;
    }


    /**
     * {@inheritdoc}
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * {@inheritdoc}
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * {@inheritdoc}
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * Returns the gender list.
     *
     * @return array
     */
    public static function getGenderList()
    {
        return [
            'gender_unknown' => UserInterface::GENDER_UNKNOWN,
            'gender_female' => UserInterface::GENDER_FEMALE,
            'gender_male' => UserInterface::GENDER_MALE,
        ];
    }

}
