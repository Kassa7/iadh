<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Form;

use Cassandra\Type\UserType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;



class EmailType extends AbstractType
{
    const GENDER_FEMALE = 'Mme';
    const GENDER_MALE = 'Mr';
    const GENDER_UNKNOWN = 'Mlle';



    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add('subject', null, array('required' => true)); //  ==>require
        $builder->add('message', null, array('required' => true));

    }


    public function getBlockPrefix()
    {
        return 'app_email';
    }

//    // For Symfony 2.x
//    public function getName()
//    {
//        return $this->getBlockPrefix();
//    }
}
