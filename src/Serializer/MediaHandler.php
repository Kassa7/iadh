<?php
namespace App\Serializer;

use App\Application\Sonata\MediaBundle\Entity\Media;
use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use Sonata\MediaBundle\Provider\MediaProviderInterface;
use Sonata\MediaBundle\Provider\Pool;

class MediaHandler implements SubscribingHandlerInterface
{
private $mediaPool;

public function __construct(Pool $mediaPool)
{
$this->mediaPool = $mediaPool;
}

public static function getSubscribingMethods()
{
return array(
array(
'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
'format'    => 'json',
'type'      => 'Application\Sonata\MediaBundle\Entity\Media',
'method'    => 'serializeToJson',
),
);
}

public function serializeToJson(JsonSerializationVisitor $visitor, Media $media, array $type, Context $context)
{
$formats = array(MediaProviderInterface::FORMAT_REFERENCE);
$formats = array_merge($formats, array_keys($this->mediaPool->getFormatNamesByContext($media->getContext())));

$provider = $this->mediaPool->getProvider($media->getProviderName());

$properties = array();
foreach ($formats as $format) {
$properties[$format]['url']        = $provider->generatePublicUrl($media, $format);
$properties[$format]['properties'] = $provider->getHelperProperties($media, $format);
}

return $properties;
}
}